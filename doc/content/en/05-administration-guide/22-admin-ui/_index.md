---
title: Admin UI
weight: 22
url: administration/adminui
description: This page contains details about how to connect on the StackGres admin UI.
---

This page contains details about how to connect on the StackGres admin UI.

{{% children style="li" depth="1" description="true" %}}